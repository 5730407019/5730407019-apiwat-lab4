<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml"/>
    <xsl:template match="/">
        <companies>
            <xsl:for-each select="companies/company">
                <company>
                    <xsl:attribute name = "symbol">
                        <xsl:value-of select="symbol"></xsl:value-of>
                    </xsl:attribute>
                    <name>
                        <xsl:value-of select="name"></xsl:value-of>
                    </name>
                   
                    <xsl:for-each select="research/labs/lab">
                        <lab>
                            <xsl:attribute name = "city">
                                <xsl:value-of select="./@location"/>
                            </xsl:attribute>
                            <xsl:value-of select="."/>
                        </lab>
                    </xsl:for-each>
                    <xsl:if test = "symbol = 'IBM'">
                        <xsl:element name="lab">
                            <xsl:attribute name="city">austin</xsl:attribute>IMB Austin Research Lab
                        </xsl:element>
                    </xsl:if>
                       
                   
                </company>
            </xsl:for-each>
           
        </companies>
    </xsl:template>

</xsl:stylesheet>
