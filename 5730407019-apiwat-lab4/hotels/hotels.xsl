<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  version="1.0"
                xmlns:apiwat="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom"
>
    <xsl:output method="html"/>
    <xsl:template match="apiwat:kml/apiwat:Document">
        <html>
            <head>
                <title>hotels.xsl</title>
            </head>
            <body>
                <h1>
                    
                    <img>
                        <xsl:attribute name = "src">
                            <xsl:value-of select = "apiwat:Style/apiwat:IconStyle/apiwat:Icon/apiwat:href"/>
                        </xsl:attribute>
                    </img>
                    <xsl:value-of select ="apiwat:name"/>
                </h1>
                <p>
                    List of  hotels
                </p>
                <xsl:for-each select = "apiwat:Placemark">
                    <ul>
                        <li>
                            <xsl:value-of select = "apiwat:name"/>
                            <ul>
                                <li>
                                    <a>
                                        <xsl:attribute name = "href">
                                            <xsl:value-of select = "substring-after(substring-before(apiwat:description,'&quot;&gt;&lt;img'),'&lt;a href=&quot;')"/>
                                        </xsl:attribute>
                                        <xsl:value-of select = "substring-after(substring-before(apiwat:description,'&quot;&gt;&lt;img'),'&lt;a href=&quot;')"/>
                                    </a>
                                </li>
                                <li>
                                    Coordinates:<xsl:value-of select = "apiwat:Point/apiwat:coordinates"/>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
